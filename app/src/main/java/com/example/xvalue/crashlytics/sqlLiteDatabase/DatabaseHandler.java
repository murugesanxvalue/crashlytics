package com.example.xvalue.crashlytics.sqlLiteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.example.xvalue.crashlytics.model.CrashlyticsModel;
import com.example.xvalue.crashlytics.model.CrashLyticsModel_DB;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Murugesan on 24-04-2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "CrashLystics";

    private static final int DATABASE_VERSION = 1;

    private static final String TAG  ="DatabaseHelper";
    /**
     * Table Name
     */
    public static final String DATABASE_CREATE_CRASH_DETAILS = "crashLysticts_details";
    /**
     * Post Table Columns crashLysticts_details
     */
    private static final String KEY_POST_ID = "id";
    private static final String KEY_POST_TITLE = "title";
    private static final String KEY_POST_USERNAME = "user_name";
    private static final String KEY_POST_AVATAR_ID = "avatar_id";
    private static final String KEY_POST_AVATAR_URL = "avatar_url";
    private static final String KEY_POST_CREATED_AT = "created_at";
    private static final String KEY_POST_UPDATE_AT = "update_at";
    private static final String KEY_POST_BODY = "body";


    private SQLiteDatabase db;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called when the database connection is being configured.
     * Configure database settings for things like foreign key support, write-ahead logging, etc.
     * @param db
     */
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }
    /**
     * Called when the database is created for the FIRST time.
     * If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CRASH_DETAILS_TABLE = "CREATE TABLE " + DATABASE_CREATE_CRASH_DETAILS +
                "(" +
                /**
                 * Define a primary key
                 */
                KEY_POST_ID+"ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
                KEY_POST_TITLE + " VARCHAR "  + "," +
                KEY_POST_USERNAME + " VARCHAR "  + "," +
                KEY_POST_AVATAR_ID + " VARCHAR" + "," + KEY_POST_AVATAR_URL + " VARCHAR" +
                "," + KEY_POST_CREATED_AT + " VARCHAR" + "," + KEY_POST_UPDATE_AT + " VARCHAR" +
                "," + KEY_POST_BODY + " VARCHAR"  +
                ")";

        db.execSQL(CREATE_CRASH_DETAILS_TABLE);


    }

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            /**
             * Simplest implementation is to drop all old tables and recreate them
             * db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
             */
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CREATE_CRASH_DETAILS);
            onCreate(db);
        }
    }
    public void insertCrashlyticsDetails(List<CrashlyticsModel> mCrashlyticsModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues initialValues = new ContentValues();
            for (CrashlyticsModel city : mCrashlyticsModel) {
                initialValues.put(KEY_POST_TITLE, city.getTitle());
                initialValues.put(KEY_POST_AVATAR_ID, city.getUser().getId());
                initialValues.put(KEY_POST_AVATAR_URL, city.getUser().getAvatar_url());
                initialValues.put(KEY_POST_USERNAME, city.getUser().getLogin());
                initialValues.put(KEY_POST_CREATED_AT, city.getCreated_at());
                initialValues.put(KEY_POST_UPDATE_AT, city.getUpdated_at());
                initialValues.put(KEY_POST_BODY, city.getBody());
                db.insert(DATABASE_CREATE_CRASH_DETAILS, null, initialValues);
            }
            db.setTransactionSuccessful();
            Log.w("DatabaseHandler", "DataBase added Successfully");
        } finally {
            db.endTransaction();
        }
    }


    public void deleteCrashlyticsDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_CREATE_CRASH_DETAILS, null, null);
    }


    /*
    * cursor 0 = ForiengnKey
    * cursor 1 = Title
    * cursor 2 = Username
    * cursor 3 = avatar_id
    * cursor 4 =avatar_url
    * cursor 5 = created_date
    * cursor 6 = update_date
    * cursor 7 = body
    * */
    public List<CrashLyticsModel_DB> getCrashlyticsModelDetails() {
        List<CrashLyticsModel_DB> mCrashlyticsModelList = new ArrayList<CrashLyticsModel_DB>();
        /**
         * Select All Query
         */
        String selectQuery = "SELECT  * FROM " + DATABASE_CREATE_CRASH_DETAILS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount() >= 0)
        {
            if (cursor.moveToFirst())
            {
                do {
                    CrashLyticsModel_DB getDistributor = new CrashLyticsModel_DB();
                    getDistributor.setTitle(cursor.getString(1));
                    getDistributor.setUsername(cursor.getString(2));
                    getDistributor.setAvatar_id(cursor.getString(3));
                    getDistributor.setAvatar_url(cursor.getString(4));
                    getDistributor.setCreated_at(cursor.getString(5));
                    getDistributor.setUpdate_at(cursor.getString(6));
                    getDistributor.setBody(cursor.getString(7));
                    /**
                     * Adding contact to list
                     */
                    mCrashlyticsModelList.add(getDistributor);
                    Log.w("DatabaseHandler", "cache:"+ getDistributor.toString());
                } while (cursor.moveToNext());
            }
        }
        return mCrashlyticsModelList;
    }



}