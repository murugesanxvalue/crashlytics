package com.example.xvalue.crashlytics.retrofitservice;

import com.example.xvalue.crashlytics.model.CrashlyticsModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Murugesan on 24-04-2018.
 */

public interface WebService {


    @GET("secureudid/issues")
    Call<List<CrashlyticsModel>> callCrashlyticsService();

}
