package com.example.xvalue.crashlytics.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.xvalue.crashlytics.R;
import com.example.xvalue.crashlytics.apputils.Utilkit;
import com.example.xvalue.crashlytics.model.CrashLyticsModel_DB;
import com.squareup.picasso.Picasso;

/**
 * Created by Murugesan on 24-04-2018.
 */

public class SingleCrashLysticsActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = getApplicationContext();
        Intent i = getIntent();
        CrashLyticsModel_DB mSelectedPositon = (CrashLyticsModel_DB) i.getSerializableExtra("sampleObject");
        ImageView imageView = (ImageView) findViewById(R.id.link_image);
        TextView user_name = (TextView) findViewById(R.id.user_name);
        TextView user_descr = (TextView) findViewById(R.id.user_descr);
        Utilkit.SetvalueInTextview(user_name, mSelectedPositon.getUsername());
        Utilkit.SetvalueInTextview(user_descr, mSelectedPositon.getBody());


        try {
            if (null != mSelectedPositon.getAvatar_url()) {
                Picasso.with(mContext)
                        .load(mSelectedPositon.getAvatar_url())
                        .placeholder(R.drawable.loading)
                        .resize(150, 150)
                        .centerCrop()
                        .into(imageView);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
