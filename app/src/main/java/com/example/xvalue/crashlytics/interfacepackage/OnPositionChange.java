package com.example.xvalue.crashlytics.interfacepackage;

import android.view.View;

public interface OnPositionChange {
    void onPositionClick(View parent, int position);
}
