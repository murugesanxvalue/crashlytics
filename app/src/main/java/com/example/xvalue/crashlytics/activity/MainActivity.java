package com.example.xvalue.crashlytics.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.example.xvalue.crashlytics.R;
import com.example.xvalue.crashlytics.adapter.RecyclerAdapter;
import com.example.xvalue.crashlytics.apputils.Utilkit;
import com.example.xvalue.crashlytics.interfacepackage.OnPositionChange;
import com.example.xvalue.crashlytics.model.CrashLyticsModel_DB;
import com.example.xvalue.crashlytics.model.CrashlyticsModel;
import com.example.xvalue.crashlytics.retrofitservice.ServiceGenerator;
import com.example.xvalue.crashlytics.retrofitservice.WebService;
import com.example.xvalue.crashlytics.sqlLiteDatabase.DatabaseHandler;
import java.util.ArrayList;
import java.util.List;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Murugesan on 24-04-2018.
 */

public class MainActivity extends AppCompatActivity implements OnPositionChange,SwipeRefreshLayout.OnRefreshListener{

    RecyclerView recycler_view;

    RecyclerAdapter recyclerAdapter;

    private LinearLayoutManager layoutManager;

    SwipeRefreshLayout swipeRefresh;

    private Context  mContext;

    List<CrashlyticsModel> crashlyticsModel;

    List<CrashLyticsModel_DB> dbcrashLytics = new ArrayList<>();

    private OnPositionChange mgetposition;

    DatabaseHandler db ;

    private TextView error_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = getApplicationContext();
        error_text=(TextView)findViewById(R.id.error_text);
        recycler_view= (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefreshlayout);
        swipeRefresh.setOnRefreshListener(this);
        layoutManager=new LinearLayoutManager(mContext);
        recycler_view.setLayoutManager(layoutManager);
        db = new DatabaseHandler(mContext);
        setListener((OnPositionChange) this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * Here getting Sqlite Database datas and Server datas
         */
        if (!Utilkit.isEmpty(DatabaseHandler.DATABASE_CREATE_CRASH_DETAILS,mContext)) {
            try{
                dbcrashLytics=  db.getCrashlyticsModelDetails() ;
                Log.i("MainActivity", "dbcrashLytics" + dbcrashLytics);
            }catch (Exception e){
                e.printStackTrace();
            }
            setRecyclerView(mContext,dbcrashLytics,mgetposition);
        }else{
            if (connectionAvailable()){
                callWebServicesCrasLlytics();
            }
            else{
                showNetDisabledAlertToUser(MainActivity.this,error_text);
            }

        }
    }

    public void setListener(OnPositionChange mOnInterfaceModels){
        this.mgetposition=mOnInterfaceModels;
    }

    /**
     * Here Calling Network API
     */
    private void callWebServicesCrasLlytics() {
        Utilkit.showSpinnerDialog(mContext, false);
        WebService webServiceObj;
        webServiceObj = ServiceGenerator.createService(WebService.class);
        Call<List<CrashlyticsModel>> call = webServiceObj.callCrashlyticsService();
        call.enqueue(new Callback<List<CrashlyticsModel>>() {
            @Override
            public void onResponse(Call<List<CrashlyticsModel>> call, Response<List<CrashlyticsModel>> response) {
                Utilkit.dismisssSpinnerDialog();
                try{
                    crashlyticsModel = response.body();

                    fetachDataFromApi(crashlyticsModel);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<CrashlyticsModel>> call, Throwable t) {
                Utilkit.dismisssSpinnerDialog();
                dismissSwipeRefresh();
            }
        });
    }

    private void fetachDataFromApi(List<CrashlyticsModel> crashlyticsModel) {
        if(crashlyticsModel!=null && !crashlyticsModel.isEmpty()){

            try {
                if (!Utilkit.isEmpty(DatabaseHandler.DATABASE_CREATE_CRASH_DETAILS,mContext)) {
                    db.deleteCrashlyticsDetails();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try{
                db.insertCrashlyticsDetails(crashlyticsModel);
            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                dbcrashLytics= db.getCrashlyticsModelDetails();
            }catch (Exception e){
                e.printStackTrace();
            }
            dismissSwipeRefresh();
            setRecyclerView(mContext,dbcrashLytics,mgetposition);


        }else{
            Log.i("onFailure", "fetachDataFromApi onFailure" );
        }


    }

    private void setRecyclerView(Context mContext, List<CrashLyticsModel_DB> dbcrashLytics, OnPositionChange mgetposition) {
        if(dbcrashLytics!=null){
            recyclerAdapter=new RecyclerAdapter(mContext,dbcrashLytics,mgetposition);
            recycler_view.setAdapter(recyclerAdapter);
            error_text.setVisibility(View.GONE);
        }else{
            error_text.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPositionClick(View parent, int position) {
        try {
            Log.i("position", "fetachDataFromApi position" + position);
            Log.i("position", "crashlyticsModel.get(position)" + dbcrashLytics.get(position));
            Intent i = new Intent(this, SingleCrashLysticsActivity.class);
            i.putExtra("sampleObject", dbcrashLytics.get(position));
            startActivity(i);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean connectionAvailable() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            /**
             * Here connected to a network
             */
            connected = true;
        }
        return connected;
    }
    public static void showNetDisabledAlertToUser(final Context context, final TextView error_text){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setMessage("Would you like to enable it?")
                .setTitle("No Internet Connection")
                .setPositiveButton(" Enable Internet ", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(dialogIntent);
                    }
                });

        alertDialogBuilder.setNegativeButton(" Cancel ", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
                error_text.setVisibility(View.VISIBLE);
                error_text.setText("No Internet Connection");
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRefresh() {
        callWebServicesCrasLlytics();
    }

    public void dismissSwipeRefresh(){
        swipeRefresh.setRefreshing(false);
    }
}
