package com.example.xvalue.crashlytics.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.example.xvalue.crashlytics.R;
import com.example.xvalue.crashlytics.apputils.Utilkit;
import com.example.xvalue.crashlytics.interfacepackage.OnPositionChange;
import com.example.xvalue.crashlytics.model.CrashLyticsModel_DB;
import com.squareup.picasso.Picasso;

/**
 * Created by Murugesan on 24-04-2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    Context mContext;

    List<CrashLyticsModel_DB> mCrashlyticsModel;

    OnPositionChange mgetposition;

    public RecyclerAdapter(Context mContext, List<CrashLyticsModel_DB> crashlyticsModel, OnPositionChange mgetposition) {
        this.mContext = mContext;
        this.mCrashlyticsModel= crashlyticsModel;
        this.mgetposition = mgetposition;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new RecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.ViewHolder holder, final int position) {

        Utilkit.SetvalueInTextview( holder.title,mCrashlyticsModel.get(position).getTitle());
        Utilkit.SetvalueInTextview( holder.user_name,mCrashlyticsModel.get(position).getUsername());
        Utilkit.SetvalueInTextview( holder.description,mCrashlyticsModel.get(position).getBody());
        try{

        if(null!=mCrashlyticsModel.get(position).getAvatar_url()) {
            Picasso.with(mContext)
                    .load(mCrashlyticsModel.get(position).getAvatar_url())
                    .placeholder(R.drawable.loading)
                    .resize(150, 150)
                    .centerCrop()
                    .into(holder.link_image);
        }
        }catch (Exception e){
            e.printStackTrace();
        }

        String mDate=mCrashlyticsModel.get(position).getUpdate_at();
        if (Utilkit.validateObjectValues(mDate)) {
            try {
                holder.date.setText(parseDate(mDate));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mgetposition.onPositionClick(v,position);
            }
        });
    }



    @Override
    public int getItemCount() {
        return mCrashlyticsModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView title,date,description,user_name;
        public final ImageView link_image;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            user_name =(TextView) view.findViewById(R.id.user_name);
            title =(TextView) view.findViewById(R.id.title);
            date=(TextView) view.findViewById(R.id.date);
            description=(TextView) view.findViewById(R.id.description);
            link_image=(ImageView)view.findViewById(R.id.link_image);

        }
    }
    public String parseDate(String time) {
        String str = null;
        DateFormat oldFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        Date oldDate = null;
        try {
            oldDate = oldFormatter .parse(time);
            str =formatter.format(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(formatter.format(oldDate));

        return str;
    }
}
