package com.example.xvalue.crashlytics.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Murugesan on 24-04-2018.
 */

public class CrashlyticsModel implements Serializable{

    private String body;

    private String closed_at;

//    private String[] assignees;

    private ArrayList<String> assignees;

    private ArrayList<String> labels;

//    private String[] labels;

    private String state;

    private String author_association;

    private String assignee;

    private String number;

    private String url;

    private String milestone;

    private String html_url;

    private String id;

    private String title;

    private String updated_at;

    private String repository_url;

    private Pull_request pull_request;

    private String comments_url;

    private String created_at;

    private String events_url;

    private String labels_url;

    private String locked;

    private User user;

    private String comments;

    public String getBody ()
    {
        return body;
    }

    public void setBody (String body)
    {
        this.body = body;
    }


    public ArrayList<String> getAssignees() {
        return assignees;
    }

    public void setAssignees(ArrayList<String> assignees) {
        this.assignees = assignees;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getAuthor_association ()
    {
        return author_association;
    }

    public void setAuthor_association (String author_association)
    {
        this.author_association = author_association;
    }



    public String getNumber ()
    {
        return number;
    }

    public void setNumber (String number)
    {
        this.number = number;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getClosed_at() {
        return closed_at;
    }

    public void setClosed_at(String closed_at) {
        this.closed_at = closed_at;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getMilestone() {
        return milestone;
    }

    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    public String getHtml_url ()
    {
        return html_url;
    }

    public void setHtml_url (String html_url)
    {
        this.html_url = html_url;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getRepository_url ()
    {
        return repository_url;
    }

    public void setRepository_url (String repository_url)
    {
        this.repository_url = repository_url;
    }

    public Pull_request getPull_request ()
    {
        return pull_request;
    }

    public void setPull_request (Pull_request pull_request)
    {
        this.pull_request = pull_request;
    }

    public String getComments_url ()
    {
        return comments_url;
    }

    public void setComments_url (String comments_url)
    {
        this.comments_url = comments_url;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getEvents_url ()
    {
        return events_url;
    }

    public void setEvents_url (String events_url)
    {
        this.events_url = events_url;
    }

    public String getLabels_url ()
    {
        return labels_url;
    }

    public void setLabels_url (String labels_url)
    {
        this.labels_url = labels_url;
    }

    public String getLocked ()
    {
        return locked;
    }

    public void setLocked (String locked)
    {
        this.locked = locked;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    public String getComments ()
    {
        return comments;
    }

    public void setComments (String comments)
    {
        this.comments = comments;
    }

    public class User implements Serializable {

        private String received_events_url;

        private String organizations_url;

        private String avatar_url;

        private String gravatar_id;

        private String gists_url;

        private String starred_url;

        private String site_admin;

        private String type;

        private String url;

        private String id;

        private String html_url;

        private String following_url;

        private String events_url;

        private String login;

        private String subscriptions_url;

        private String repos_url;

        private String followers_url;

        public String getReceived_events_url ()
        {
            return received_events_url;
        }

        public void setReceived_events_url (String received_events_url)
        {
            this.received_events_url = received_events_url;
        }

        public String getOrganizations_url ()
        {
            return organizations_url;
        }

        public void setOrganizations_url (String organizations_url)
        {
            this.organizations_url = organizations_url;
        }

        public String getAvatar_url ()
        {
            return avatar_url;
        }

        public void setAvatar_url (String avatar_url)
        {
            this.avatar_url = avatar_url;
        }

        public String getGravatar_id ()
        {
            return gravatar_id;
        }

        public void setGravatar_id (String gravatar_id)
        {
            this.gravatar_id = gravatar_id;
        }

        public String getGists_url ()
        {
            return gists_url;
        }

        public void setGists_url (String gists_url)
        {
            this.gists_url = gists_url;
        }

        public String getStarred_url ()
        {
            return starred_url;
        }

        public void setStarred_url (String starred_url)
        {
            this.starred_url = starred_url;
        }

        public String getSite_admin ()
        {
            return site_admin;
        }

        public void setSite_admin (String site_admin)
        {
            this.site_admin = site_admin;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        public String getUrl ()
        {
            return url;
        }

        public void setUrl (String url)
        {
            this.url = url;
        }

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getHtml_url ()
        {
            return html_url;
        }

        public void setHtml_url (String html_url)
        {
            this.html_url = html_url;
        }

        public String getFollowing_url ()
        {
            return following_url;
        }

        public void setFollowing_url (String following_url)
        {
            this.following_url = following_url;
        }

        public String getEvents_url ()
        {
            return events_url;
        }

        public void setEvents_url (String events_url)
        {
            this.events_url = events_url;
        }

        public String getLogin ()
        {
            return login;
        }

        public void setLogin (String login)
        {
            this.login = login;
        }

        public String getSubscriptions_url ()
        {
            return subscriptions_url;
        }

        public void setSubscriptions_url (String subscriptions_url)
        {
            this.subscriptions_url = subscriptions_url;
        }

        public String getRepos_url ()
        {
            return repos_url;
        }

        public void setRepos_url (String repos_url)
        {
            this.repos_url = repos_url;
        }

        public String getFollowers_url ()
        {
            return followers_url;
        }

        public void setFollowers_url (String followers_url)
        {
            this.followers_url = followers_url;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [received_events_url = "+received_events_url+", organizations_url = "+organizations_url+", avatar_url = "+avatar_url+", gravatar_id = "+gravatar_id+", gists_url = "+gists_url+", starred_url = "+starred_url+", site_admin = "+site_admin+", type = "+type+", url = "+url+", id = "+id+", html_url = "+html_url+", following_url = "+following_url+", events_url = "+events_url+", login = "+login+", subscriptions_url = "+subscriptions_url+", repos_url = "+repos_url+", followers_url = "+followers_url+"]";
        }
    }


    @Override
    public String toString()
    {
        return "ClassPojo [body = "+body+", closed_at = "+closed_at+", assignees = "+assignees+", labels = "+labels+", state = "+state+", author_association = "+author_association+", assignee = "+assignee+", number = "+number+", url = "+url+", milestone = "+milestone+", html_url = "+html_url+", id = "+id+", title = "+title+", updated_at = "+updated_at+", repository_url = "+repository_url+", pull_request = "+pull_request+", comments_url = "+comments_url+", created_at = "+created_at+", events_url = "+events_url+", labels_url = "+labels_url+", locked = "+locked+", user = "+user+", comments = "+comments+"]";
    }
}
